﻿using System;
using System.Text;
using System.Collections.Generic;

namespace Task2
{
    class Program
    {
        /// <summary>
        /// Connection each symbol and count of them in text
        /// </summary>
        class CharCount
        {
            public char Symbol { get; set; }        
            public int Count { get; private set; }
            /// <summary>
            /// Default constructor. First count of current symbol is one.
            /// </summary>
            /// <param name="symbol"></param>
            public CharCount(char symbol)
            {
                Symbol = symbol;
                Count = 1;
            }
            /// <summary>
            /// Increment
            /// </summary>
            /// <param name="c">Our object</param>
            /// <returns>New object with increment count</returns>
            public static CharCount operator ++(CharCount c)
            {
                CharCount obj = new CharCount(c.Symbol)
                {
                    Count = c.Count + 1
                };
                return obj;
            }
            /// <summary>
            /// Equals
            /// </summary>
            /// <param name="obj">Our object</param>
            /// <param name="c">symbol</param>
            /// <returns>True, if object symbol is equal parameter, else false</returns>
            public static bool operator ==(CharCount obj, char c)
            {
                return (obj.Symbol == c) ? true : false;
            }
            public static bool operator !=(CharCount obj, char c)
            {
                return (obj.Symbol != c) ? true : false;
            }
        }
        /// <summary>
        /// Static class for Render the object CharCount
        /// </summary>
        static class CharCountRender
        {
            /// <summary>
            /// Render on the screen
            /// </summary>
            /// <param name="obj">Current Object</param>
            public static void Render(CharCount obj)        
            {
                if (obj != null)
                {
                    Console.WriteLine($"Symbol {obj.Symbol}: {obj.Count}");
                }
            }
        }
        static void Main(string[] args)
        {
            Console.WriteLine("Enter the string: ");
            string str = Console.ReadLine();
            StringBuilder strBuilder = new StringBuilder(str);  //For better optimization
            List<CharCount> list = new List<CharCount>();       //List our symbols
            bool isReady;
            int num;
            for (int i = 0; i < strBuilder.Length; i++)
            {
                isReady = true;
                for (num = 0; num < list.Count; num++)  //Finding used symbols in list
                {
                    if (list[num] == strBuilder[i])     
                    {
                        isReady = false;
                        break;
                    }
                }
                if (isReady) list.Add(new CharCount(strBuilder[i]));        //Adding new CharCount in list
                else list[num]++;       //Increment the count of current symbol
            }
            foreach (CharCount el in list) CharCountRender.Render(el);  //Render on the screen
        }
    }
}
