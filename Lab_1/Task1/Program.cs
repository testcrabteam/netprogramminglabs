﻿using System;

namespace Task1
{

    class Program
    {
        /// <summary>
        /// Render the matrix on Console screen
        /// </summary>
        /// <param name="matrix">your matrix</param>
        public static void MatrixRender(int[,] matrix)
        {
            if (matrix != null)
            {
                for (int i = 0; i < matrix.GetUpperBound(0) + 1; i++)
                {
                    for (int j = 0; j < matrix.GetUpperBound(1) + 1; j++)
                    {
                        Console.Write("{0, 4:d}", matrix[i, j]); 
                    }
                    Console.WriteLine();
                }
            }
        }
        /// <summary>
        /// Count sums the elements lying parallel to the main diagonal
        /// </summary>
        /// <param name="matrix">Matrix for calculating</param>
        /// <returns>Array sums each diagonal</returns>
        public static int[] MatrixSumParallelMain(int[,] matrix)
        {
            int[] sum = null;
            if (matrix != null && matrix.GetUpperBound(0) > 0)
            {
                int size = matrix.GetUpperBound(0) + 1;
                int lastInd = size - 1;
                sum = new int[2 * (size - 1)];
                for (int i = 0; i < lastInd; i++)
                {
                    for (int j = 0; j <= i; j++)
                    {
                        sum[i] += matrix[i - j, j];     //Along the diagonal
                        sum[sum.Length - i - 1] += matrix[lastInd - j, lastInd - i + j];   
                    }
                }
            }
            return sum;
        }
        static void Main(string[] args)
        {
            Random rand = new Random();
            Console.WriteLine("Enter the size of matrix: ");
            int size = int.Parse(Console.ReadLine());
            if (size > 0)
            {
                int[,] mat = new int[size, size];
                Console.WriteLine("Executing the random generator...");
                for (int i = 0; i < mat.GetUpperBound(0) + 1; i++)
                {
                    for (int j = 0; j < mat.GetUpperBound(1) + 1; j++)
                    {
                        mat[i, j] = rand.Next(-25, 25);             //Random generation
                    }
                }
                MatrixRender(mat);
                Console.WriteLine("That's your result matrix. Press any key to find sum of parallel main diagonal elements...");
                int[] res = MatrixSumParallelMain(mat);
                Console.WriteLine("Your sum is: ");
                foreach (int el in res)
                {
                    Console.WriteLine($"{el,4}");           //Render results on the screen
                }
            }
            else Console.WriteLine("Error!");
            Console.ReadKey();
        }
    }
}
