﻿using System;
using System.Text.RegularExpressions;

namespace Task3
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Enter the text or HTML-code:");
            string str = Console.ReadLine();

            /*Creation HTML-format:
             <html>
                (any spaces)
                <h1>Any text</h1>
                <form>Any text</form>
                (any spaces)
             </html>*/
            Regex regex = new Regex
                (@"^<html>\s*<h1>\s*\w*\s*</h1>\s*<form>\s*\w*\s*</form>\s*</html>$");

            //Become matches collection with format by regex by entered string
            MatchCollection matches = regex.Matches(str);
            if (matches.Count > 0)          //If there are matches
                Console.WriteLine("That's true HTML-code! That's form with header!");
            else
                Console.WriteLine("That's usual text or no valid HTML-code!");           //If there aren't matches
        }
    }
}
