﻿/*
    Проект является собственностью разработчика известного как Mister Class.
    Распространение исходных файлов должно происходить только с его разрешения.
    Ибо нечего тырить чужие исходники, надо которыми он старается.
    Попытки стырить исходники карается штрафом, от злобного взгляда до пинка.
    Mister Class © 2019 All rights reserved.
    Task: 
    Для заданной структуры данных разработать абстрактный класс и класс-наследник. 
    В классе реализовать несколько конструкторов. Создать методы, работающие с полями класса. 
    Часть из них должны быть виртуальными. Добавить методы-свойства и индексаторы.
    
    Структура данных ИЗДЕЛИЕ: название, шифр, количество, комплектация.
 */
// <copyright file="Program.cs" company="No">
// Copyright (c) 2019 All Rights Reserved
// </copyright>
// <author>Mister Class</author>
// <date>10/06/2019 12:49:28 </date>
// <summary>Main Program</summary>

using System;

namespace Lab2
{
    abstract class Product
    {
        private static int lastKey = 0;
        private string name;
        private int key;
        private int count;
        private string[] complect;

        public string Name
        {
            get
            {
                return name;
            }
            protected set
            {
                name = value;
            }
        }

        public int Key
        {
            get
            {
                return key;
            }
            private set
            {
                key = value;
            }
        }

        public int Count
        {
            get
            {
                return count;
            }
            set
            {
                count = Math.Abs(value);
            }
        }

        public string[] Complect
        {
            get
            {
                return complect;
            }
            set
            {
                complect = (value != null) ? value.Clone() as string[] : null;
            }
        }
        public string this[int ind]
        {
            get
            {
                if (ind > 0 && ind < complect.Length)
                {
                    return complect[ind - 1];
                }
                return null;
            }
            set
            {
                if (ind > 0 && ind < complect.Length)
                {
                    complect[ind - 1] = value;
                }
            }
        }

        public Product()
        {
            Key = ++lastKey;
            Count = 0;
            Name = "Undefined";
            Complect = null;
        }
        public Product(string _name, string[] _complect) : this()
        {
            Complect = _complect;
            Name = _name;
        }
        /// <summary>
        /// Replace component in complect by index
        /// </summary>
        /// <param name="component"></param>
        /// <param name="ind"></param>
        public void Update(string component, int ind)
        {
            if (this[ind] != null)
            {
                this[ind] = component;
            }
        }
        /// <summary>
        /// Increment Count
        /// </summary>
        public void Add()
        {
            Count++;
        }
        /// <summary>
        /// Use product
        /// </summary>
        public virtual void Use()
        {
            Console.WriteLine("Product is used");
        }
        /// <summary>
        /// Print info on the screen
        /// </summary>
        public abstract void Print();
    }
}
