﻿using System;

namespace Lab2
{
    class ProductException : Exception
    {
        private int id;

        public int Id
        {
            get;
            private set;
        }
        public ProductException(string message, int id) : base(message)
        {
            Id = id;
        }
    }
}
