﻿/*
    Проект является собственностью разработчика известного как Mister Class.
    Распространение исходных файлов должно происходить только с его разрешения.
    Ибо нечего тырить чужие исходники, надо которыми он старается.
    Попытки стырить исходники карается штрафом, от злобного взгляда до пинка.
    Mister Class © 2019 All rights reserved.
    Task: 
    2. Для заданной структуры данных разработать абстрактный класс и класс-наследник. 
    В классе реализовать несколько конструкторов. Создать методы, работающие с полями класса. 
    Часть из них должны быть виртуальными. Добавить методы-свойства и индексаторы.
    3. Разработать интерфейсные классы, добавляющие некоторые ме¬тоды в класс-потомок. 
    Изучить причины возникновения коллизий имен при наследовании и способы их устранения.
    4. Разработать классы исключительных ситуаций и применить их для обработки, возникающих исключений.
    
    Структура данных ИЗДЕЛИЕ: название, шифр, количество, комплектация.
 */
// <copyright file="Program.cs" company="No">
// Copyright (c) 2019 All Rights Reserved
// </copyright>
// <author>Mister Class</author>
// <date>10/06/2019 12:49:28 </date>
// <summary>Main Program</summary>

using System;

namespace Lab2
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                Product pr = new Book("Games", new string[] { "Pen", "Authograph" }, "MisterClass");
                pr.Add();
                pr.Use();
                pr.Update("Big Pen!", 1);
                pr.Print();

                IOpen open = pr as IOpen;
                IKey key = pr as IKey;
                //open.Open();
                key.Hint();
                key.Open();
                open.Close();
            }
            catch(ProductException ex)
            {
                Console.WriteLine(ex.Message);
                Console.WriteLine($"Ключ продукта: {ex.Id}");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
    }
}
