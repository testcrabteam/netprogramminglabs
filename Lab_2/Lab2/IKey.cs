﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lab2
{
    interface IKey
    {
        void Open();
        void Hint();
    }
}
