﻿using System;

namespace Lab2
{
    sealed class Book : Product, IOpen, IKey
    {
        public string Author { get; set; }
        public bool IsOpened { get; private set; } = false;
        public Book() : base()
        {
            Author = "Undefined";
        }
        public Book(string _name, string[] _complect, string author)
            : base(_name, _complect)
        {
            Author = author;
        }
        public override void Use()
        {
            Console.WriteLine("The book have been reading!");
        }
        public override void Print()
        {
            Console.WriteLine("-----------------");
            Console.WriteLine($"Название: {this.Name}");
            Console.WriteLine($"Автор: {this.Author}");
            Console.WriteLine($"Количество: {this.Count}");
            Console.WriteLine($"Уникальный ключ продукта: {this.Key}");
            Console.Write("В комплекте: ");
            if (this.Complect != null)
            {
                foreach (string el in this.Complect)
                    Console.Write($"{el} ");
                Console.WriteLine();
            }
            else Console.WriteLine("Ничего!");

            Console.WriteLine("-----------------");
        }

        void IOpen.Open()
        {
            IsOpened = true;
            Console.WriteLine("Открываем книгу...");
        }

        void IOpen.Close()
        {
            IsOpened = false;
            Console.WriteLine("Закрываем книгу...");
        }

        void IKey.Open()
        {
            if (IsOpened) Console.WriteLine("Снимаем печать!");
            else throw new ProductException("Нельзя снять печать, не прочитав книгу!", Key);
        }

        void IKey.Hint()
        {
            Console.WriteLine("Книга используется для снятия печати безумия!");
        }
    }
}
